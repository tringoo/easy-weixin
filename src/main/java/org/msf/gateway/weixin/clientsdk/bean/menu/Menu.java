package org.msf.gateway.weixin.clientsdk.bean.menu;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

public class Menu {
	private String name;
	private String type;
	private String key;
	private String url;
	@JSONField(name = "sub_button")
	private List<Menu> subButton;

	public Menu() {
	}

	public Menu(String name, String type, String key) {
		super();
		this.name = name;
		this.type = type;
		this.key = key;
	}

	public Menu(String name, String url) {
		super();
		this.name = name;
		this.type = "view";
		this.url = url;
	}

	public Menu(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setSubButton(List<Menu> subButton) {
		this.subButton = subButton;
	}

	public List<Menu> getSubButton() {
		return subButton;
	}

	public void addSubMenu(Menu subMenu) {
		if (subButton == null) {
			subButton = new ArrayList<Menu>();
		}
		subButton.add(subMenu);
	}
}

package org.msf.gateway.weixin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.msf.gateway.weixin.clientsdk.Config;

@SuppressWarnings("serial")
public class SetServlet extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		process(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) {
		try {
			String pic = req.getParameter("pic");
			Config.setPic(pic);
			resp.getWriter().write("ok");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

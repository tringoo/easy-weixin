package org.msf.gateway.weixin.in.message;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.constant.InMsgType;

public abstract class InMessage {
	private String toUserName;
	private String fromUserName;
	private String createTime;
	private byte msgType = 0x00;
	private String msgTypeStr;

	public InMessage(Element root) throws DocumentException {
		this.setToUserName(root.elementText("ToUserName"));
		this.setFromUserName(root.elementText("FromUserName"));
		this.setCreateTime(root.elementText("CreateTime"));
		this.setMsgTypeStr(root.elementText("MsgType"));
		this.setMsgType(InMsgType.get(this.msgTypeStr));
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public byte getMsgType() {
		return msgType;
	}

	public void setMsgType(byte msgType) {
		this.msgType = msgType;
	}

	public String getMsgTypeStr() {
		return msgTypeStr;
	}

	public void setMsgTypeStr(String msgTypeStr) {
		this.msgTypeStr = msgTypeStr;
	}
}

package org.msf.gateway.weixin.in.message.normal;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.NormalMessage;

public class VideoMessage extends NormalMessage {

	private String thumbMediaId;
	private String mediaId;

	public VideoMessage(Element root) throws DocumentException {
		super(root);
		this.setThumbMediaId(root.elementText("ThumbMediaId"));
		this.setMediaId(root.elementText("MediaId"));
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "VideoMessage [thumbMediaId=" + thumbMediaId + ", mediaId=" + mediaId + "]";
	}

}

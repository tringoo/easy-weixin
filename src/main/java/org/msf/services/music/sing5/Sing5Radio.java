package org.msf.services.music.sing5;

import java.io.IOException;

import org.msf.gateway.weixin.clientsdk.Config;
import org.msf.gateway.weixin.clientsdk.WeixinApi;
import org.msf.gateway.weixin.clientsdk.WeixinError;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;

import com.alibaba.fastjson.JSON;

public class Sing5Radio {
	public static String SING5_RADIO_URL_FMT = "http://5sing.kugou.com/fm/fm/index?command=rand&type=%s&page=1";

	public static void handle(String channel, String fromUserName) {
		Sing5List sing5list = null;
		// List<CustomNew> news = null;
		try {
			sing5list = JSON.parseObject(WebUtils.doGet(String.format(SING5_RADIO_URL_FMT, channel)), Sing5List.class);
			int i = 0;
			// news = new ArrayList<CustomNew>();
			for (Song song : sing5list.getDatas()) {
				if (++i > 5)
					break;
				// news.add(new CustomNew(song.getName(), song.getSinger(),
				// song.getSingerImg(), song.getSingerImg()));
				WeixinApi.sendCustomMusic(song.getName(), song.getSinger(), song.getFile(), Config.getPic(), fromUserName);
			}
			// System.out.println(WeixinApi.sendCustomNews("o3nzAjpBrCaJl7YFfWewpEPpIE8E",
			// news));
		} catch (IOException e) {
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		} finally {
			sing5list = null;
			// if (news != null)
			// news.clear();
			// news = null;
		}
	}
}

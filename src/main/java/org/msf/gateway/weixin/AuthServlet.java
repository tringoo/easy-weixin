package org.msf.gateway.weixin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.msf.gateway.weixin.clientsdk.WeixinApi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

//授权回调
@SuppressWarnings("serial")
public class AuthServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(req, resp);
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println(JSON.toJSONString(req.getParameterMap()));
		try {
			String code = req.getParameter("code");
			// 根据code得到access_token
			JSONObject oauth2AccessToken = WeixinApi.oauth2_accesstoken(code);
			// 根据access_token得到用户信息
			String uinfo = WeixinApi.oauth2_userinfo(oauth2AccessToken.getString("access_token"), oauth2AccessToken.getString("openid"));
			resp.getWriter().write(uinfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package org.msf.gateway.weixin.in;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.msf.gateway.weixin.constant.InEventType;
import org.msf.gateway.weixin.constant.InMsgType;
import org.msf.gateway.weixin.in.handler.InHandler;
import org.msf.gateway.weixin.in.message.EventMessage;
import org.msf.gateway.weixin.in.message.event.LocationEvent;
import org.msf.gateway.weixin.in.message.event.MenuEvent;
import org.msf.gateway.weixin.in.message.event.ScanEvent;
import org.msf.gateway.weixin.in.message.normal.ImageMessage;
import org.msf.gateway.weixin.in.message.normal.LocationMessage;
import org.msf.gateway.weixin.in.message.normal.TextMessage;
import org.msf.gateway.weixin.in.message.normal.VideoMessage;
import org.msf.gateway.weixin.in.message.normal.VoiceMessage;

public class InHandlerRunnable implements Runnable {
	private String xml = null;
	private InHandler handler = null;

	public InHandlerRunnable(String xml, InHandler handler) {
		this.xml = xml;
		if (handler == null)
			throw new RuntimeException("无效的handler");
		this.handler = handler;
	}

	public void run() {
		try {
			Document doc = DocumentHelper.parseText(xml);
			Element root = doc.getRootElement();
			// 可以存放在数据库里面根据appid获取handle
			byte msgType = InMsgType.get(root.elementText("MsgType"));
			switch (msgType) {
			case InMsgType.TEXT:
				this.handler.text(new TextMessage(root));
				break;
			case InMsgType.IMAGE:
				this.handler.image(new ImageMessage(root));
				break;
			case InMsgType.VOICE:
				this.handler.voice(new VoiceMessage(root));
				break;
			case InMsgType.VIDEO:
				this.handler.video(new VideoMessage(root));
				break;
			case InMsgType.LOCATION:
				this.handler.location(new LocationMessage(root));
				break;
			case InMsgType.LINK:
				this.handler.text(new TextMessage(root));
				break;
			case InMsgType.EVENT:
				byte eventType = InEventType.get(root.elementText("Event").toLowerCase());
				switch (eventType) {
				case InEventType.FOLLOW:
					if (root.element("EventKey") != null) {
						this.handler.onScanSubscribe(new ScanEvent(root));
					} else {
						this.handler.onSubscribe(new EventMessage(root));
					}
					break;
				case InEventType.UNFOLLOW:
					this.handler.onUnSubscribe(new EventMessage(root));
					break;
				case InEventType.SCAN:
					this.handler.onScan(new ScanEvent(root));
					break;
				case InEventType.LOCATION:
					this.handler.onLocation(new LocationEvent(root));
					break;
				case InEventType.CLICK:
					this.handler.onMenuClick(new MenuEvent(root));
					break;
				case InEventType.VIEW:
					this.handler.onMenuView(new MenuEvent(root));
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		} catch (DocumentException e) {
			System.out.println("无效的xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

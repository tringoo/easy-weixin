package org.msf.services.music.sing5;

public class Song {
	private String singer;// 歌手
	private String name;// 歌曲名
	private String singerImg;// 封面
	private String file;// 歌曲地址

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSingerImg() {
		return singerImg;
	}

	public void setSingerImg(String singerImg) {
		this.singerImg = singerImg;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "Song [singer=" + singer + ", name=" + name + ", singerImg=" + singerImg + ", file=" + file + "]";
	}

}

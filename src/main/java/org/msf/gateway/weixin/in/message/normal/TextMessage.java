package org.msf.gateway.weixin.in.message.normal;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.NormalMessage;

public class TextMessage extends NormalMessage {

	private String content;

	public TextMessage(Element root) throws DocumentException {
		super(root);
		this.setContent(root.elementText("Content"));
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "TextMessage [content=" + content + "]";
	}

}

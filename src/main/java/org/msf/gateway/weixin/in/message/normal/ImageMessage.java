package org.msf.gateway.weixin.in.message.normal;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.NormalMessage;

public class ImageMessage extends NormalMessage {

	private String picUrl;
	private String mediaId;

	public ImageMessage(Element root) throws DocumentException {
		super(root);
		this.setPicUrl(root.elementText("PicUrl"));
		this.setMediaId(root.elementText("MediaId"));
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "ImageMessage [picUrl=" + picUrl + ", mediaId=" + mediaId + "]";
	}

}

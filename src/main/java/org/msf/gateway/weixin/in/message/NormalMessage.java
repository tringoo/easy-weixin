package org.msf.gateway.weixin.in.message;

import org.dom4j.DocumentException;
import org.dom4j.Element;

public class NormalMessage extends InMessage {

	private String msgId;

	public NormalMessage(Element root) throws DocumentException {
		super(root);
		this.setMsgId(root.elementText("MsgId"));
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
}

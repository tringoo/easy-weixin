package org.msf.gateway.weixin.in.message;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.constant.InEventType;

public class EventMessage extends InMessage {

	private byte eventType = 0x00;
	private String eventTypeStr;

	public EventMessage(Element root) throws DocumentException {
		super(root);
		this.setEventTypeStr(root.elementText("Event"));
		this.setEventType(InEventType.get(this.eventTypeStr.toLowerCase()));
	}

	public byte getEventType() {
		return eventType;
	}

	public void setEventType(byte eventType) {
		this.eventType = eventType;
	}

	public String getEventTypeStr() {
		return eventTypeStr;
	}

	public void setEventTypeStr(String eventTypeStr) {
		this.eventTypeStr = eventTypeStr;
	}
}

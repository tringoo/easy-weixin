package org.msf.services.life;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.msf.gateway.weixin.clientsdk.WeixinError;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;
import org.msf.services.TextResponse;

import com.alibaba.fastjson.JSON;

public class MobileAreaService extends TextResponse {
	// 淘宝手机归属地API
	public static final String API_TAOBAO = "http://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=%s";
	public static final Pattern MESSAGE_PATTERN = Pattern.compile("手机((13[0-9]|15[0|1|2|3|5|6|7|8|9]|18[0,1,2,5-9])\\d{4,8})");
	private Matcher matcher = null;

	// 查询手机归属地
	public static String getMobileArea(String telephone) throws IOException {
		String rs = null;
		try {
			rs = WebUtils.doGet(String.format(API_TAOBAO, telephone));
			return JSON.parseObject(rs.replace("__GetZoneResult_ = ", "")).getString("carrier");
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public String handler(String text, String fromuser) {
		try {
			return getMobileArea(matcher.group(1));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean matches(String text) {
		matcher = MESSAGE_PATTERN.matcher(text);
		return matcher.find();
	}

}

package org.msf.gateway.weixin.in.message.event;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.EventMessage;

public class ScanEvent extends EventMessage {

	private String eventKey;
	private String ticket;

	public ScanEvent(Element root) throws DocumentException {
		super(root);
		this.setEventKey(root.elementText("EventKey"));
		this.setTicket(root.elementText("Ticket"));
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Override
	public String toString() {
		return "ScanEvent [eventKey=" + eventKey + ", ticket=" + ticket + "]";
	}

}

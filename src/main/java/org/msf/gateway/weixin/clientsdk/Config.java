package org.msf.gateway.weixin.clientsdk;

public class Config {
	public static final String APP_ID = "wx7401049541e0d4a9";// "wx7fe43202458e5090";
	public static final String APP_SECRET = "49d0ae947f9d757deb77f3dd127d6afb";// "53df4bf286409b53bb9a90b82caef0c8";
	// public static final String fm_pic =
	// "Nnb13hngL_Srh1GThSzDaMYVin99I3bKQKgPtHD0HRkjY3kox3h4gP8Q7NM-TJ2-";
	private static String fm_pic = "bhg_s0nsAAvP88si2mnolEBrLyObrEGExLu_ptcupAQJ57AJFnYoPuDJ7bJ8kafe";

	public static String getPic() {
		return fm_pic;
	}

	public static void setPic(String pic) {
		fm_pic = pic;
	}

	// 获取AccessToken
	public static final String URLFMT_ACCESSTOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
	// 获取关注者列表
	public static final String URLFMT_USER_GET = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=";
	// 获取用户基本信息
	public static final String URLFMT_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN";
	// 上传图片、语音、视频
	public static final String URLFMT_MEDIA_UPLOAD = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";
	// 下载图片
	public static final String URLFMT_MEDIA_GET = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";
	// 群发
	public static final String URLFMT_MESSAGE_MASS_SEND = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=%s";
	// 发送客服消息
	public static final String URLFMT_MESSAGE_CUSTOM_SEND = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=%s";
	// 高级群发接口 - 上传图文消息素材
	public static final String URLFMT_MEDIA_UPLOADNEWS = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=%s";
	// 高级群发接口 - 分组群发
	public static final String URLFMT_MESSAGE_MASS_SENDALL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=%s";
	// 自定义菜单 - 查询
	public static final String URLFMT_MENU_GET = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=%s";
	public static final String URLFMT_MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s";
	// 二维码（临时或永久）
	public static final String URLFMT_QRCODE_CREATE = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";
	public static final String URLFMT_QRCODE_SHOW = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s";
	// 语义理解（查询天气，火车，电影等）
	public static final String URLFMT_SEARCH = "https://api.weixin.qq.com/semantic/semproxy/search?access_token=%s";
	// oauth2获取access_token
	public static final String URLFMT_OAUTH2_ACCESSTOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
	// oauth2获取用户信息
	public static final String URLFMT_OAUTH2_USERINFO = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s";
	// 添加客服
	public static final String URLFMT_KFACCOUNT_ADD = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token=%s";
	// 获取客服基本信息
	public static final String URLFMT_GETKFLIST = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=%s";

}

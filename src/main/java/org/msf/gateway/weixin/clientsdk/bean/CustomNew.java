package org.msf.gateway.weixin.clientsdk.bean;

public class CustomNew {
	private String title;
	private String description;
	private String linkUrl;
	private String picUrl;

	public CustomNew() {

	}

	public CustomNew(String title, String description, String linkUrl, String picUrl) {
		this.title = title;
		this.description = description;
		this.linkUrl = linkUrl;
		this.picUrl = picUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

}

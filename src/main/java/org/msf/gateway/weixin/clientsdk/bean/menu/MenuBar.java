package org.msf.gateway.weixin.clientsdk.bean.menu;

import java.util.ArrayList;
import java.util.List;

public class MenuBar {
	private List<Menu> button;

	public List<Menu> getButton() {
		return button;
	}

	public void add(Menu subMenu) {
		if (button == null) {
			button = new ArrayList<Menu>();
		}
		button.add(subMenu);
	}
}

package org.msf.gateway.weixin.in.handler;

import org.msf.gateway.weixin.in.message.EventMessage;
import org.msf.gateway.weixin.in.message.event.LocationEvent;
import org.msf.gateway.weixin.in.message.event.MenuEvent;
import org.msf.gateway.weixin.in.message.event.ScanEvent;
import org.msf.gateway.weixin.in.message.normal.ImageMessage;
import org.msf.gateway.weixin.in.message.normal.LinkMessage;
import org.msf.gateway.weixin.in.message.normal.LocationMessage;
import org.msf.gateway.weixin.in.message.normal.TextMessage;
import org.msf.gateway.weixin.in.message.normal.VideoMessage;
import org.msf.gateway.weixin.in.message.normal.VoiceMessage;

public interface InHandler {
	public abstract void text(TextMessage message);

	public abstract void image(ImageMessage message);

	public abstract void voice(VoiceMessage message);

	public abstract void video(VideoMessage message);

	public abstract void location(LocationMessage message);

	public abstract void link(LinkMessage message);

	public abstract void onSubscribe(EventMessage event);

	public abstract void onUnSubscribe(EventMessage event);

	public abstract void onScanSubscribe(ScanEvent event);

	public abstract void onScan(ScanEvent event);

	public abstract void onLocation(LocationEvent event);

	public abstract void onMenuClick(MenuEvent event);

	public abstract void onMenuView(MenuEvent event);
}

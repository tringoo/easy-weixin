package org.msf.gateway.weixin.clientsdk.bean;

import com.alibaba.fastjson.annotation.JSONField;

public class Oauth2AccessToken {
	@JSONField(name = "access_token")
	private String accessToken;
	@JSONField(name = "expires_in")
	private Integer expiresIn;

	@JSONField(name = "refresh_token")
	private Integer refreshToken;

	@JSONField(name = "openid")
	private Integer openid;

	@JSONField(name = "scope")
	private Integer scope;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	public Integer getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(Integer refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Integer getOpenid() {
		return openid;
	}

	public void setOpenid(Integer openid) {
		this.openid = openid;
	}

	public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

}

package org.msf.gateway.weixin.in.message.event;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.EventMessage;

public class LocationEvent extends EventMessage {

	// 地理位置纬度
	private String latitude;
	// 地理位置经度
	private String longitude;
	// 地理位置精度
	private String precision;

	public LocationEvent(Element root) throws DocumentException {
		super(root);
		this.setLatitude(root.elementText("Latitude"));
		this.setLongitude(root.elementText("Longitude"));
		this.setPrecision(root.elementText("Precision"));
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPrecision() {
		return precision;
	}

	public void setPrecision(String precision) {
		this.precision = precision;
	}

	@Override
	public String toString() {
		return "LocationEvent [latitude=" + latitude + ", longitude=" + longitude + ", precision=" + precision + "]";
	}

}

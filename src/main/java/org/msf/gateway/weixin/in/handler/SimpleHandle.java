package org.msf.gateway.weixin.in.handler;

import org.msf.gateway.weixin.clientsdk.WeixinApi;
import org.msf.gateway.weixin.in.message.EventMessage;
import org.msf.gateway.weixin.in.message.event.LocationEvent;
import org.msf.gateway.weixin.in.message.event.MenuEvent;
import org.msf.gateway.weixin.in.message.event.ScanEvent;
import org.msf.gateway.weixin.in.message.normal.ImageMessage;
import org.msf.gateway.weixin.in.message.normal.LinkMessage;
import org.msf.gateway.weixin.in.message.normal.LocationMessage;
import org.msf.gateway.weixin.in.message.normal.TextMessage;
import org.msf.gateway.weixin.in.message.normal.VideoMessage;
import org.msf.gateway.weixin.in.message.normal.VoiceMessage;
import org.msf.services.TextResponse;

public abstract class SimpleHandle implements InHandler {

	public void text(TextMessage message) {
		// System.out.println("处理文字消息..." + message);
		handlerText(message.getContent(), message.getFromUserName());
	}

	public void image(ImageMessage message) {
		// System.out.println("处理图片消息..." + message);
	}

	public void voice(VoiceMessage message) {
		// System.out.println("处理声音消息..." + message);
		handlerText(message.getRecognition(), message.getFromUserName());
	}

	public void video(VideoMessage message) {
		// System.out.println("处理视频消息..." + message);
	}

	public void location(LocationMessage message) {
		// System.out.println("处理地理位置消息..." + message);
	}

	public void link(LinkMessage message) {
		// System.out.println("处理link消息..." + message);
	}

	public void onSubscribe(EventMessage event) {
		// System.out.println("处理onSubscribe消息..." + event.getFromUserName());
		// handlerSubscribe(event);
		onSubscribe(event.getFromUserName());
	}

	public void onUnSubscribe(EventMessage event) {
		// System.out.println("处理onUnSubscribe消息..." + event.getFromUserName());
	}

	public void onScanSubscribe(ScanEvent event) {
		// System.out.println("处理onScanSubscribe消息...");
		// handlerSubscribe(event);
		onSubscribe(event.getFromUserName());
	}

	public void onScan(ScanEvent event) {
		// System.out.println("处理onScan消息..." + event);
	}

	public void onLocation(LocationEvent event) {
		// System.out.println("处理onLocation消息..." + event);
	}

	public void onMenuClick(MenuEvent event) {
		// System.out.println("处理onMenuClick消息...");
		// String key = event.getEventKey();
		// if (key.indexOf("DOUBANFM_") != -1) {
		// DoubanRadio.handle(key.split("_")[1], event.getFromUserName());
		// return;
		// }
		// if (key.indexOf("5SINGFM_") != -1) {
		// Sing5Radio.handle(key.split("_")[1], event.getFromUserName());
		// return;
		// }
		onMenuClick(event.getFromUserName(), event.getEventKey());
	}

	protected abstract void onMenuClick(String fromUser, String menuKey);

	public void onMenuView(MenuEvent event) {
		// System.out.println("打开链接地址:" + event.getEventKey());
	}

	// 关注
	// private void handlerSubscribe(EventMessage event) {
	// WeixinApi.sendCustomText(event.getFromUserName(),
	// "欢迎关注，回复以下关键字(文本或语音)即可查询到相应信息：冷笑话，xx(国内城市)天气 ，手机xxxxxxxxxxx(国内手机号)");
	// // System.out.println(rs);
	// }

	// 处理文本
	private void handlerText(String text, String fromuser) {
		if (text == null || text.isEmpty())
			return;
		TextResponse[] textHandlers = getTextHandlers();
		if (textHandlers == null || textHandlers.length == 0)
			return;
		for (TextResponse textHandler : textHandlers) {
			if (textHandler.matches(text)) {
				String result = textHandler.handler(text, fromuser);
				if (result == null || result.isEmpty())
					return;
				WeixinApi.sendCustomText(fromuser, result);
				return;
			}
		}
	}

	// 子类注册所有文本处理类
	protected abstract TextResponse[] getTextHandlers();

	protected abstract void onSubscribe(String fromUser);
}

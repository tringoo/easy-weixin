package org.msf.gateway.weixin.clientsdk;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import org.msf.gateway.weixin.clientsdk.bean.CustomNew;
import org.msf.gateway.weixin.clientsdk.bean.FanInfo;
import org.msf.gateway.weixin.clientsdk.bean.FansList;
import org.msf.gateway.weixin.clientsdk.bean.Media;
import org.msf.gateway.weixin.clientsdk.bean.menu.Menu;
import org.msf.gateway.weixin.clientsdk.bean.menu.MenuBar;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class WeixinApi {

	private static final char[] DIGITS_LOWER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	private static String TOKEN = "weixin-gateway";

	// 加密
	public static String sha1Hex(String instr) {
		try {
			byte[] data = MessageDigest.getInstance("SHA-1").digest(instr.getBytes("UTF-8"));
			int l = data.length;
			char[] out = new char[l << 1];
			// two characters form the hex value.
			for (int i = 0, j = 0; i < l; i++) {
				out[j++] = DIGITS_LOWER[(0xF0 & data[i]) >>> 4];
				out[j++] = DIGITS_LOWER[0x0F & data[i]];
			}
			return new String(out);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Invalid algorithm.");
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static boolean validateSignature(String timestamp, String nonce, String signature) {
		// 排序
		String[] strarr = { timestamp, nonce, TOKEN };
		Arrays.sort(strarr);
		StringBuilder sb = new StringBuilder();
		for (String str : strarr) {
			sb.append(str);
		}
		strarr = null;
		// 加密
		String _signature = sha1Hex(sb.toString()); // DigestUtils.sha1Hex(sb.toString());
		sb.setLength(0);
		return _signature.equals(signature);
	}

	// get请求
	private static String doGetAutoAccessToken(String apifmt_url, Object... fmtargs) {
		try {
			Object[] args = new Object[fmtargs.length + 1];
			args[0] = AccessTokenFactory.getAccessTokenStr();
			int i = 1;
			for (Object arg : fmtargs) {
				args[i++] = arg;
			}
			return WebUtils.doGet(String.format(apifmt_url, args));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	// post请求
	private static String dopostJson(String apifmt_url, String jsonstr) {
		try {
			return WebUtils.doPostJson(String.format(apifmt_url, AccessTokenFactory.getAccessTokenStr()), jsonstr);
		} catch (IOException e) {
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private static Media uploadImage(String imgpath) {
		return WebUtils.doPostMedia(String.format(Config.URLFMT_MEDIA_UPLOAD, AccessTokenFactory.getAccessTokenStr(), "image"), imgpath);
	}

	private static Media uploadVoice(String voicepath) {
		return WebUtils.doPostMedia(String.format(Config.URLFMT_MEDIA_UPLOAD, AccessTokenFactory.getAccessTokenStr(), "voice"), voicepath);
	}

	private static Media uploadVideo(String videopath) {
		return WebUtils.doPostMedia(String.format(Config.URLFMT_MEDIA_UPLOAD, AccessTokenFactory.getAccessTokenStr(), "video"), videopath);
	}

	// 获取关注者列表
	public static String[] getFansList() {
		String fansListJsonStr = doGetAutoAccessToken(Config.URLFMT_USER_GET);
		System.out.println(fansListJsonStr);
		if (fansListJsonStr == null)
			return null;
		FansList fansList = JSON.parseObject(fansListJsonStr, FansList.class);
		return fansList.getOpenidList();
	}

	// 获取关注者基本信息
	public static FanInfo getFanInfo(String openid) {
		String faninfoJsonStr = doGetAutoAccessToken(Config.URLFMT_USER_INFO, openid);
		return JSON.parseObject(faninfoJsonStr, FanInfo.class);
	}

	// 创建菜单
	public static String createMenuBar(String jsonstr) {
		return dopostJson(Config.URLFMT_MENU_CREATE, jsonstr);
	}

	// 获取菜单
	public static String getMenuList() {
		return doGetAutoAccessToken(Config.URLFMT_MENU_GET);
	}

	// 发送客服消息 - 文本
	public static String sendCustomText(String userOpenId, String text) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"touser\":\"").append(userOpenId).append("\",\"msgtype\":\"text\",\"text\":{\"content\":\"").append(text).append("\"}}");
			return dopostJson(Config.URLFMT_MESSAGE_CUSTOM_SEND, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 发送客服消息 - 媒体- 已经上传过的
	public static String sendCustomRemoteMedia(String mediaType, String userOpenId, String media_id) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"touser\":\"").append(userOpenId).append("\",\"msgtype\":\"").append(mediaType).append("\",\"").append(mediaType)
					.append("\":{\"media_id\":\"").append(media_id).append("\"}}");
			return dopostJson(Config.URLFMT_MESSAGE_CUSTOM_SEND, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 发送客服消息 - 单个图文(微信要求限制在10条以内，超过10则将会无响应)
	public static String sendCustomNew(String userOpenId, String title, String description, String linkUrl, String picUrl) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"touser\":\"").append(userOpenId).append("\",\"msgtype\":\"news\",\"news\":{\"articles\": [{\"title\":\"").append(title)
					.append("\",\"description\":\"").append(description).append("\",\"url\":\"").append(linkUrl).append("\",\"picurl\":\"").append(picUrl)
					.append("\"}]}}");
			return dopostJson(Config.URLFMT_MESSAGE_CUSTOM_SEND, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 发送客服消息 - 多个图文(微信要求限制在10条以内，超过10则将会无响应)
	public static String sendCustomNews(String userOpenId, List<CustomNew> news) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"touser\":\"").append(userOpenId).append("\",\"msgtype\":\"news\",\"news\":{\"articles\": [");
			int i = 0;
			for (CustomNew cnew : news) {
				i++;
				sb.append("{\"title\":\"").append(cnew.getTitle()).append("\",\"description\":\"").append(cnew.getDescription()).append("\",\"url\":\"")
						.append(cnew.getLinkUrl()).append("\",\"picurl\":\"").append(cnew.getPicUrl()).append("\"}");
				if (i != news.size())
					sb.append(",");
			}
			sb.append("]}}");
			return dopostJson(Config.URLFMT_MESSAGE_CUSTOM_SEND, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 发送客服消息 - 图片
	public static String sendCustomLocalImage(String userOpenId, String localImage) {
		return sendCustomRemoteMedia("image", userOpenId, uploadImage(localImage).getMedia_id());
	}

	// 发送客服消息 - 语音
	public static String sendCustomLocalVoice(String userOpenId, String localVoice) {
		return sendCustomRemoteMedia("voice", userOpenId, uploadVoice(localVoice).getMedia_id());
	}

	// 发送客服消息 - 音乐
	public static String sendCustomMusic(String title, String description, String music_url, String thumb_media_id, String userOpenId) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"touser\":\"").append(userOpenId).append("\",\"msgtype\":\"music\",\"music\":{\"title\":\"").append(title)
					.append("\",\"description\":\"").append(description).append("\",\"musicurl\":\"").append(music_url).append("\",\"hqmusicurl\":\"")
					.append(music_url).append("\",\"thumb_media_id\":\"").append(thumb_media_id).append("\"}}");
			return dopostJson(Config.URLFMT_MESSAGE_CUSTOM_SEND, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 发送客服消息 - mp4视频
	public static String sendCustomLocalVideo(String userOpenId, String localVideo) {
		return sendCustomRemoteMedia("video", userOpenId, uploadVideo(localVideo).getMedia_id());
	}

	// 推广支持 - 创建二维码
	public static String createQrcode(String sceneId) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"action_name\":\"QR_LIMIT_SCENE\",\"action_info\":{\"scene\":{\"scene_id\":").append(sceneId).append("}}}");
			return dopostJson(Config.URLFMT_QRCODE_CREATE, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 添加客服
	public static String addKfaccount(String kf_account, String nickname, String password) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"kf_account\":\"").append(kf_account).append("\",\"nickname\":\"").append(nickname).append("\",\"password\":\"").append(password)
					.append("\"}");
			System.out.println(sb.toString());
			return dopostJson(Config.URLFMT_KFACCOUNT_ADD, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	// 获取客服基本信息
	public static String getKfList() {
		return doGetAutoAccessToken(Config.URLFMT_GETKFLIST);
	}

	// 智能接口 - 语义理解
	public static String search(String query, String category) {
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("{\"query\":\"").append(query).append("\",\"category\":\"").append(category).append("\",\"appid\":\"").append(Config.APP_ID).append("\"");
			return dopostJson(Config.URLFMT_SEARCH, sb.toString());
		} finally {
			sb.setLength(0);
		}
	}

	public static JSONObject oauth2_accesstoken(String code) throws IOException {
		try {
			String rs = WebUtils.doGet(String.format(Config.URLFMT_OAUTH2_ACCESSTOKEN, Config.APP_ID, Config.APP_SECRET, code));
			System.out.println("-->" + rs);
			return JSON.parseObject(rs);
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static String oauth2_userinfo(String access_token, String openid) throws IOException {
		try {
			return WebUtils
					.doGet(String
							.format(Config.URLFMT_OAUTH2_USERINFO,
									"OezXcEiiBSKSxW0eoylIeBB_ppgtsj7U3d7omtxPf8-rjYVIjYhrzhOSM3aD2-gmE_fwOMdh5LSGqryW5PHO7PKElEQUVNw6_ySkbg77d-MyKhjaW2mZd_2UrWTBi5wX7e9Jt-if0m60_EyaCwq_tw",
									openid));
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static void main(String[] args) {
		// String[] fanids = getFansList();
		// for (String fanid : fanids) {
		// System.out.println(getFanInfo(fanid));

		// System.out.println(createMenuBar(buildMenuBar()));
		System.out.println(getMenuList());
		// gh_b40ed269f5be
		// System.out.println(addKfaccount("100@gh_b40ed269f5be", "test",
		// MD5.md5("123456")));
		// System.out.println(getKfList());
		// System.out.println(sendCustomMusic("那些你很冒险的梦", "林俊杰",
		// "http://mr4.douban.com/201412311448/afcf23a205dc91f5c476a40df28a7340/view/song/small/p1772391.mp4",
		// "Nnb13hngL_Srh1GThSzDaMYVin99I3bKQ KgPtHD0HRkjY3kox3h4gP8Q7NM-TJ2-",
		// "o3nzAjpBrCaJl7YFfWewpEPpIE8E"));

		// System.out.println(getFanInfo("o3nzAjpBrCaJl7YFfWewpEPpIE8E"));

		// System.out.println(sendCustomText("o3nzAjpBrCaJl7YFfWewpEPpIE8E","推送文本消息111..."));
		// 上传图片
		// System.out.println(uploadImage("H:/photo/cyx.jpg").getMedia_id());

		// 推送图片
		// System.out.println(sendCustomLocalImage("o3nzAjpBrCaJl7YFfWewpEPpIE8E",
		// "H:/photo/cyx.jpg"));

		// 推送语音
		// System.out.println(sendCustomLocalVoice("o3nzAjpBrCaJl7YFfWewpEPpIE8E",
		// "H:/photo/qwj.mp3"));

		// 推送视频
		// System.out.println(sendCustomLocalVideo("o3nzAjpBrCaJl7YFfWewpEPpIE8E",
		// "H:/photo/wx.mp4"));

		// 推送新闻
		// System.out.println(sendCustomNews("o3nzAjpBrCaJl7YFfWewpEPpIE8E",
		// "授权测试", "授权...",
		// "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx7401049541e0d4a9&redirect_uri=http%3A%2F%2Fwxtest.tunnel.mobi%2Fweixingateway%2Fauth&response_type=code&scope=snsapi_userinfo&state=STATE",
		// "http://t100.qpic.cn/mblogpic/eddd4d2eaffdda39819e/2000.jpg"));

		// System.out.println(createQrcode("ZFB_YECX"));

		// System.out.println(search("查一下明天从北京到上海的南航机票","flight"));
	}

	public static String buildMenuBar() {
		MenuBar bar = new MenuBar();

		Menu m1 = new Menu("豆瓣电台");
		m1.addSubMenu(new Menu("华语MHz", "click", "DOUBANFM_1"));
		m1.addSubMenu(new Menu("欧美MHz", "click", "DOUBANFM_2"));
		m1.addSubMenu(new Menu("八零MHz", "click", "DOUBANFM_4"));
		m1.addSubMenu(new Menu("九零MHz", "click", "DOUBANFM_5"));
		m1.addSubMenu(new Menu("粤语MHz", "click", "DOUBANFM_6"));
		bar.add(m1);

		Menu m2 = new Menu("5sing电台");
		m2.addSubMenu(new Menu("华语MHz", "click", "5SINGFM_20"));
		m2.addSubMenu(new Menu("欧美MHz", "click", "5SINGFM_26"));
		m2.addSubMenu(new Menu("古风MHz", "click", "5SINGFM_28"));
		m2.addSubMenu(new Menu("纯音乐MHz", "click", "5SINGFM_25"));
		m2.addSubMenu(new Menu("老歌MHz", "click", "5SINGFM_22"));
		bar.add(m2);

		Menu m3 = new Menu("bar2", "http://ogo5slzeovv1r0_weixin.tunnel.mobi/weixingateway/bar.html");
		bar.add(m3);
		return JSON.toJSONString(bar);
	}

}

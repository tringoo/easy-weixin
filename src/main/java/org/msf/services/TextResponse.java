package org.msf.services;

public abstract class TextResponse {

	// 满足条件匹配则执行handler
	public abstract boolean matches(String text);

	public abstract String handler(String text, String fromuser);

}

package org.msf.services.life;

import java.io.IOException;

import org.msf.gateway.weixin.clientsdk.WeixinError;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;
import org.msf.services.TextResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class ColdJokeService extends TextResponse {
	// 百度冷笑话API
	public static final String API_BAIDU = "http://api.open.baidu.com/pae/aese/api/search?from_mid=1&format=json&ie=utf-8&oe=utf-8&subtitle=%E7%AC%91%E8%AF%9D&Cookie=3E5458D6769D0A85AE97A1DF1DDEEAAC&query=%E7%AC%91%E8%AF%9D&stat0=%E6%96%87%E5%AD%97&rn=1&t=1408414251.116&pn=6&srcid=4031";

	// 查询冷笑话
	public static String getJoke() throws IOException {
		JSONArray results = null;
		try {
			results = JSON.parseObject(WebUtils.doGet(API_BAIDU)).getJSONArray("data");
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		if (results == null)
			return "";
		JSONObject disp_data = results.getJSONObject(0).getJSONArray("disp_data").getJSONObject(0);
		StringBuilder sb = new StringBuilder();
		try {
			sb.append("【笑话分类】 ").append(disp_data.getString("cate")).append("\n");
			sb.append("【笑话评分】 ").append(disp_data.getString("score")).append("\n");
			sb.append("【笑话访问量】 ").append(disp_data.getString("pv")).append("\n");
			sb.append("【笑话标题】 ").append(disp_data.getString("title")).append("\n");
			sb.append("【笑话内容】 ").append(disp_data.getString("content")).append("\n");
			return sb.toString();
		} finally {
			sb.setLength(0);
		}

	}

	@Override
	public String handler(String text, String fromuser) {
		try {
			return getJoke();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean matches(String text) {
		return "冷笑话".equals(text);
	}

}

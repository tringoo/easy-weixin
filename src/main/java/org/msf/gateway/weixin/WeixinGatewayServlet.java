package org.msf.gateway.weixin;

import java.io.IOException;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.msf.gateway.weixin.clientsdk.WeixinApi;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;
import org.msf.gateway.weixin.in.InHandlerRunnable;
import org.msf.gateway.weixin.in.handler.InHandler;

@SuppressWarnings("serial")
public class WeixinGatewayServlet extends HttpServlet {
	// gh_b40ed269f5be
	// appid：wx7401049541e0d4a9
	// appsecret：49d0ae947f9d757deb77f3dd127d6afb
	// URL：http://ogo5slzeovv1r0_weixin.tunnel.mobi/weixingateway/v1
	// URL：http://wxgateway.sturgeon.mopaas.com/v1
	private InHandler handler;

	@SuppressWarnings("unchecked")
	@Override
	public void init() throws ServletException {
		super.init();
		try {
			String handlerClassName = getInitParameter("handler");
			Class<? extends InHandler> handlerClass = (Class<? extends InHandler>) Class.forName(handlerClassName);
			this.handler = handlerClass.newInstance();
		} catch (NullPointerException e) {
			throw new ServletException("在init-param的param-name中找不到handler");
		} catch (ClassNotFoundException e) {
			throw new ServletException("配置的handler类找不到：" + e.getMessage());
		} catch (ClassCastException e) {
			throw new ServletException("配置的handler不是有效的InHandler子类：" + e.getMessage());
		} catch (InstantiationException e) {
			throw new ServletException(e);
		} catch (IllegalAccessException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String timestamp = req.getParameter("timestamp");
		if (timestamp == null || timestamp.isEmpty())
			return;
		String nonce = req.getParameter("nonce");
		if (nonce == null || nonce.isEmpty())
			return;
		String signature = req.getParameter("signature");
		if (signature == null || signature.isEmpty())
			return;
		String echostr = req.getParameter("echostr");
		if (echostr == null || echostr.isEmpty())
			return;
		if (WeixinApi.validateSignature(timestamp, nonce, signature))
			resp.getWriter().write(echostr);
	}

	@Override
	protected void doPost(HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		String timestamp = req.getParameter("timestamp");
		if (timestamp == null || timestamp.isEmpty())
			return;
		String nonce = req.getParameter("nonce");
		if (nonce == null || nonce.isEmpty())
			return;
		String signature = req.getParameter("signature");
		if (signature == null || signature.isEmpty())
			return;
		if (!WeixinApi.validateSignature(timestamp, nonce, signature))
			return;
		try {
			Executors.newSingleThreadExecutor().execute(new InHandlerRunnable(IOUtils.toString(req.getInputStream(), WebUtils.DEFAULT_CHARSET), this.handler));
			resp.getWriter().write("");
		} catch (Exception e) {
		} finally {
			// QH.free();
		}
	}

}

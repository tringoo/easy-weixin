package org.msf.services.life;

import java.io.IOException;
import java.net.URLEncoder;

import org.msf.gateway.weixin.clientsdk.WeixinError;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;
import org.msf.services.TextResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class WeatherService extends TextResponse {
	// 百度天气API
	public static final String API_BAIDU = "http://api.map.baidu.com/telematics/v3/weather?location=%s&output=json&ak=1F3FbTOWSRgKfGARU7wbrdtc";

	@Override
	public boolean matches(String text) {
		return text.matches(".+天气");
	}

	@Override
	public String handler(String text, String fromuser) {
		try {
			int idx = text.indexOf("天气");
			return getWeather(text.substring(0, idx));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 查询天气
	public static String getWeather(String city) throws IOException {
		JSONArray results = null;
		try {
			results = JSON.parseObject(WebUtils.doGet(String.format(API_BAIDU, URLEncoder.encode(city, WebUtils.DEFAULT_CHARSET)))).getJSONArray("results");
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
		if (results == null)
			return "";
		JSONObject data = results.getJSONObject(0);
		StringBuilder result = new StringBuilder();
		try {
			result.append("【PM2.5指数】").append(data.getString("pm25")).append("\n");
			JSONArray indexArr = data.getJSONArray("index");
			for (Object o : indexArr) {
				JSONObject index = (JSONObject) o;
				result.append("【" + index.getString("tipt") + "】").append(index.getString("des")).append("\n");
			}
			result.append("\n【当前及未来天气】\n");
			JSONArray wArr = data.getJSONArray("weather_data");
			for (Object o : wArr) {
				JSONObject w = (JSONObject) o;
				result.append(w.getString("date")).append("：").append(w.getString("weather")).append("，").append(w.getString("temperature")).append("\n");
			}
			return result.toString();
		} finally {
			result.setLength(0);
		}
	}

}

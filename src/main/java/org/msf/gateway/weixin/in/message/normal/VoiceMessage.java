package org.msf.gateway.weixin.in.message.normal;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.NormalMessage;

public class VoiceMessage extends NormalMessage {

	private String format;
	private String mediaId;
	// 语音识别结果
	private String recognition;

	public VoiceMessage(Element root) throws DocumentException {
		super(root);
		this.setFormat(root.elementText("Format"));
		this.setMediaId(root.elementText("MediaId"));
		this.setRecognition(root.elementText("Recognition"));
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getRecognition() {
		return recognition;
	}

	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	@Override
	public String toString() {
		return "VoiceMessage [format=" + format + ", mediaId=" + mediaId + ", recognition=" + recognition + "]";
	}

}

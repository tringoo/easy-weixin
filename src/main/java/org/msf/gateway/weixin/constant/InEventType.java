package org.msf.gateway.weixin.constant;

public class InEventType {
	public static final byte FOLLOW = 0x01;// 1.关注事件 - 用户关注服务窗
	public static final byte UNFOLLOW = 0x02;// 2.取消关注事件 - 用户取消关注服务窗
	public static final byte LOCATION = 0x03;// 3.上报地理位置事件
	public static final byte CLICK = 0x04;// 4.菜单点击事件 - 用户点击某个菜单项
	public static final byte VIEW = 0x05;// 菜单为链接跳转URL类型 。
	public static final byte SCAN = 0x06;

	public static byte get(String key) {
		if ("subscribe".equals(key))
			return FOLLOW;
		if ("unsubscribe".equals(key))
			return UNFOLLOW;
		if ("location".equals(key))
			return LOCATION;
		if ("click".equals(key))
			return CLICK;
		if ("view".equals(key))
			return VIEW;
		if ("scan".equals(key))
			return SCAN;
		return 0x00;
	}
}

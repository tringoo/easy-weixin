package org.msf.gateway.weixin.in.message.normal;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.NormalMessage;

public class LinkMessage extends NormalMessage {

	private String title;
	private String description;
	private String url;

	public LinkMessage(Element root) throws DocumentException {
		super(root);
		this.setTitle(root.elementText("Title"));
		this.setDescription(root.elementText("Description"));
		this.setUrl(root.elementText("Url"));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "LinkMessage [title=" + title + ", description=" + description + ", url=" + url + "]";
	}

}

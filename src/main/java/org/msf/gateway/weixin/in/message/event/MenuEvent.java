package org.msf.gateway.weixin.in.message.event;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.EventMessage;

public class MenuEvent extends EventMessage {

	private String eventKey;

	public MenuEvent(Element root) throws DocumentException {
		super(root);
		this.setEventKey(root.elementText("EventKey"));
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	@Override
	public String toString() {
		return "MenuEvent [eventKey=" + eventKey + "]";
	}

}

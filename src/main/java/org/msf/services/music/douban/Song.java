package org.msf.services.music.douban;

public class Song {
	private String artist;// 歌手
	private String title;// 歌曲名
	private String picture;// 封面
	private String url;// 歌曲地址

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Song [artist=" + artist + ", title=" + title + ", picture=" + picture + ", url=" + url + "]";
	}

}

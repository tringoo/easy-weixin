package org.msf.services.music.douban;

import java.io.IOException;

import org.msf.gateway.weixin.clientsdk.Config;
import org.msf.gateway.weixin.clientsdk.WeixinApi;
import org.msf.gateway.weixin.clientsdk.WeixinError;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;

import com.alibaba.fastjson.JSON;

public class DoubanRadio {
	public static String DOUBAN_RADIO_URL_FMT = "http://douban.fm/j/mine/playlist?type=n&sid=&pt=0.0&channel=%s&from=radio&r=";

	public static void handle(String channel, String fromUserName) {
		DouList doulist = null;
		try {
			doulist = JSON.parseObject(WebUtils.doGet(String.format(DOUBAN_RADIO_URL_FMT, channel)), DouList.class);
			for (Song song : doulist.getSong())
				WeixinApi.sendCustomMusic(song.getTitle(), song.getArtist(), song.getUrl(), Config.getPic(), fromUserName);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		} finally {
			doulist = null;
		}
	}
}

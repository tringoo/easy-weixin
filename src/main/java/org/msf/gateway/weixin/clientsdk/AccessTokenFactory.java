package org.msf.gateway.weixin.clientsdk;

import java.io.IOException;

import org.msf.gateway.weixin.clientsdk.bean.AccessToken;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;

public class AccessTokenFactory {
	private static AccessToken accessToken;

	public static String getAccessTokenStr() {
		return "0Xe85Wyo3luuPCuqI-l6FSNmPVFuSC0RW6SX4djS5a3IE-4kHgYFWkcUgePyMGqZqlvMM8ljzWgFskS7qn6O7QdM7R8K5O9pgnohp1JvZLo";

		// String tk = accessToken().getAccessToken();
		// System.out.println(tk);
		// return tk;
		// return accessToken().getAccessToken();
	}

	// 获取access token
	public static AccessToken accessToken() {
		if (accessToken != null && accessToken.isAvailable())
			return accessToken;
		refreshAccessToken();
		return accessToken;
	}

	public static void refreshAccessToken() {
		accessToken = requestAccessToken();
	}

	private static synchronized AccessToken requestAccessToken() {
		AccessToken result = null;
		for (int i = 0; i < 3; i++) {
			String json = null;
			try {
				json = WebUtils.doGet(String.format(Config.URLFMT_ACCESSTOKEN, Config.APP_ID, Config.APP_SECRET));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (WeixinError e) {
				System.out.println(e.getMessage());
			}
			result = new AccessToken(json);
			if (result.isAvailable())
				break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
		return result;
	}
}

package org.msf.gateway.weixin.in.message.normal;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.msf.gateway.weixin.in.message.NormalMessage;

public class LocationMessage extends NormalMessage {

	private String locationX;
	private String locationY;
	private String scale;
	private String label;

	public LocationMessage(Element root) throws DocumentException {
		super(root);
		this.setLocationX(root.elementText("Location_X"));
		this.setLocationY(root.elementText("Location_Y"));
		this.setScale(root.elementText("Scale"));
		this.setLabel(root.elementText("Label"));
	}

	public String getLocationX() {
		return locationX;
	}

	public void setLocationX(String locationX) {
		this.locationX = locationX;
	}

	public String getLocationY() {
		return locationY;
	}

	public void setLocationY(String locationY) {
		this.locationY = locationY;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "LocationMessage [locationX=" + locationX + ", locationY=" + locationY + ", scale=" + scale + ", label=" + label + "]";
	}

}

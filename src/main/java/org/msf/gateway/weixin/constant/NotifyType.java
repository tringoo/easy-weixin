package org.msf.gateway.weixin.constant;

public class NotifyType {
	public static final byte EVENT_ENTER = 0x01;
	public static final byte EVENT_FOLLOW = 0x02;
	public static final byte EVENT_UNFOLLOW = 0x03;
	public static final byte EVENT_MENU_CLICK = 0x04;
	public static final byte TEXT = 0x05;

	public static byte get(byte inMsgType, byte inEventType) {
		if (InMsgType.EVENT == inMsgType && InEventType.CLICK == inEventType)
			return EVENT_MENU_CLICK;
		return 0x00;
	}
}

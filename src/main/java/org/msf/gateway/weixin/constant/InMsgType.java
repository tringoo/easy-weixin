package org.msf.gateway.weixin.constant;

public final class InMsgType {
	public static final byte EVENT = 0x1;
	public static final byte TEXT = 0x2;
	public static final byte IMAGE = 0x3;
	public static final byte VOICE = 0x4;
	public static final byte VIDEO = 0x5;
	public static final byte LOCATION = 0x6;
	public static final byte LINK = 0x7;

	public static byte get(String key) {
		if ("event".equals(key))
			return EVENT;
		if ("text".equals(key))
			return TEXT;
		if ("image".equals(key))
			return IMAGE;
		if ("voice".equals(key))
			return VOICE;
		if ("video".equals(key))
			return VIDEO;
		if ("location".equals(key))
			return LOCATION;
		if ("link".equals(key))
			return LINK;
		return 0x00;
	}
}

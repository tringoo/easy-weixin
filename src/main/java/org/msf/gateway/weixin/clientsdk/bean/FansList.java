package org.msf.gateway.weixin.clientsdk.bean;

public class FansList {
	private int total;
	private int count;
	private FansArray data;
	private String next_openid;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public FansArray getData() {
		return data;
	}

	public void setData(FansArray data) {
		this.data = data;
	}

	public String getNext_openid() {
		return next_openid;
	}

	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}

	// getOpenid list
	public String[] getOpenidList() {
		return data == null ? new String[] {} : data.getOpenid();
	}

	private class FansArray {
		private String[] openid;

		public String[] getOpenid() {
			return openid;
		}

		@SuppressWarnings("unused")
		public void setOpenid(String[] openid) {
			this.openid = openid;
		}
	}
}
